## Estructura de carpeta FreeRTOS

Las rutas exactas pueden variar según la versión y la organización del proyecto. 
A continuación, se muestra la disposición de carpetas para el proyecto FreeRTOS: 

- `/FreeRTOS` : Código fuente y archivos de configuración de FreeRTOS
  - `/Source` : Código fuente de FreeRTOS
    - `/include` : Archivos de cabecera de FreeRTOS
      - `FreeRTOS.h` : Archivo de cabecera principal de FreeRTOS
      - `task.h` : Archivo de cabecera para la API de tareas de FreeRTOS
      - `queue.h` : Archivo de cabecera para la API de colas de FreeRTOS
      - `semphr.h` : Archivo de cabecera para la API de semáforos de FreeRTOS
    - `/portable` : Código fuente del puerto de FreeRTOS
      - `/GCC` : Puerto de FreeRTOS para el compilador GCC
        - `/ARM_CM3` : Puerto de FreeRTOS para ARM Cortex-M3 (que es el tipo de núcleo en STM32F303K8)
          - `port.c` : Archivo de código fuente del puerto
          - `portmacro.h` : Archivo de cabecera del puerto
  - `/Demo` : Ejemplos y configuraciones específicas del proyecto
    - `/CORTEX_M3_STM32F303_Nucleo` : Carpeta para de proyecto específico
      - `FreeRTOSConfig.h` : Archivo de configuración de FreeRTOS para el proyecto
      - `main.c` : Archivo principal de la aplicación
      - `startup_stm32f303x8.s` : Archivo de arranque en ensamblador para el STM32F303K8
      - `system_stm32f3xx.c` : Archivo de inicialización del sistema para el STM32F303K8
	  
El archivo FreeRTOSConfig.h es una parte esencial de cualquier proyecto de FreeRTOS. Este archivo define varias 
configuraciones importantes, como las prioridades de interrupción, la frecuencia de reloj del sistema, el tamaño del 
stack, entre otros.

Debido a que estas configuraciones son específicas para el proyecto y para el núcleo STM32F303K8, 
este archivo se encuentra en la carpeta de tu proyecto específico (/Demo/CORTEX_M3_STM32F303_Nucleo en este caso).







