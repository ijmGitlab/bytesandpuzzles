# Bienvenido a mi repositorio

¡Hola! Soy [Iván Jaraiz Mateo](https://www.linkedin.com/in/ivanjaraizmateo/), un apasionado desarrollador de software y 
entusiasta de la tecnología. Este es mi repositorio de proyectos, donde comparto algunas de mis creaciones y aprendizajes.


# Juego de Comunicaciones - UART, SPI, I2C

![Logo del Juego](/assets/logo.png)

[Wiki BytesAndPuzzles](https://gitlab.com/ijmGitlab/bytesandpuzzles/-/wikis/home)

Bienvenido al Juego de Comunicaciones, donde te enfrentarás a emocionantes desafíos y aventuras relacionados con los protocolos de comunicación UART, SPI e I2C. Explora un mundo de dispositivos electrónicos y microcontroladores mientras resuelves acertijos y desbloqueas nuevos niveles.

## Instrucciones de Juego

- 🕹️ Elige tu personaje y aventúrate en un viaje a través de diferentes escenarios y niveles.
- 📡 Utiliza UART para comunicarte con otros personajes y recibir pistas para avanzar en el juego.
- 🌐 Interactúa con dispositivos conectados a través de SPI para resolver desafiantes rompecabezas.
- 🔌 Domina la comunicación I2C para desbloquear secretos ocultos y obtener recompensas especiales.
- 🎮 Supera los obstáculos y enemigos utilizando tus habilidades de programación y resolución de problemas.

## Tecnologías utilizadas

- 🐍 Implementado en Python para una experiencia de juego interactivo y fácil prototipado.
- 💻 Utilización de C# y C++ para la lógica del juego y la interfaz de usuario.
- 📡 Comunicación UART, SPI e I2C simulada mediante bibliotecas y emuladores.

## Estructura del Repositorio

- `/bytesandpuzzles` : Raíz del repositorio
  - `/FreeRTOS` : Código fuente y archivos de configuración de FreeRTOS
	- `FreeRTOSConfig.h` : Archivo de configuración de FreeRTOS
  - `/docs` : Documentación del proyecto, instrucciones, etc.
	- `UserGuide.md` : Guía del usuario del juego
  - `/src` : Código fuente del proyecto
    - `/game_logic` : Código C++ para la lógica del juego
      - `/include` : Encabezados .h para C++
		- `GameEngine.h` : Archivo de encabezado para el motor del juego
      - `/src` : Archivos de código fuente .cpp para C++
		- `GameEngine.cpp` : Archivo de implementación para el motor del juego
    - `/business_layer` : Código C# para la capa de negocio
		- `BusinessLogic.cs` : Archivo de código para la capa de negocio
    - `/communication` : Código Python para la comunicación UART, SPI e I2C
		- `UART.py` : Archivo de código para la comunicación UART
		- `SPI.py` : Archivo de código para la comunicación SPI
		- `I2C.py` : Archivo de código para la comunicación I2C
  - `/test` : Códigos de pruebas unitarias, de integración, etc.
	- `game_logic_test.cpp` : Archivo de pruebas unitarias para la lógica del juego
  - `/libs` : Bibliotecas externas, si las hay
  - `/tools` : Herramientas auxiliares, scripts de compilación, etc.
	- `build.sh` : Script de compilación
  - `/assets` : Recursos del juego (imágenes, sonidos, etc.)
	- `logo.png` : Recurso de imagen para el juego
  - `.gitignore` : Lista de archivos y carpetas a ignorar por Git
  - `README.md` : Descripción del proyecto, cómo construirlo, etc.
  - `LICENSE` : Licencia del proyecto
  
  Los nombres de los archivos y las estructuras de las carpetas pueden variar según las necesidades del 
  proyecto. También las subcarpetas dentro de estas carpetas principales, dependiendo de la complejidad. 

## Configuración del entorno de desarrollo

Este proyecto utiliza Python, C# y C++ y se recomienda los siguientes IDEs:

- Para Python, utilizar PyCharm o Jupyter Notebooks.
- Para C#, utilizar Visual Studio o Rider.
- Para C++, utilizar Visual Studio o CLion.
- Para FreeRTOS, utilizar Eclipse con el complemento de desarrollo de sistemas embebidos.

## Bibliotecas necesarias

Además, este proyecto utiliza varias bibliotecas que necesitarás instalar. Aquí están las bibliotecas necesarias para cada lenguaje:

- Python:
  - PySerial
  - SPIDEV
  - SMBus
  - pygame
  - pytest
  
- C#:
  - .NET Core o .NET 5/6
  - Moq & xUnit
  - System.IO.Ports
  
- C++:
  - Boost ASIO
  - Google Test
  - SPIdev library
  - i2c-tools

- FreeRTOS:
  - FreeRTOS kernel

## Instalación y Ejecución

1. Clona este repositorio en tu máquina local.
2. Asegúrate de tener instalados Python, C# y C++ en tu sistema.
3. Sigue las instrucciones en la carpeta de cada lenguaje para compilar y ejecutar el juego.

## Contribuciones

¡Contribuciones son bienvenidas!

Si te apasionan las comunicaciones y la programación en Python, C# y C++, ¡únete a nosotros y contribuye en el desarrollo de este emocionante juego! Esperamos recibir nuevas ideas, mejoras y correcciones de errores para hacer de este juego una experiencia aún más increíble.

## Licencia

Proyecto personal - Todos los derechos reservados.

Este es un proyecto personal y no se proporciona ninguna licencia explícita para su uso, distribución o modificación. Todos los derechos sobre el código, diseño y contenido del proyecto están reservados por el autor.

El propósito de compartir este repositorio en GitLab es con fines educativos y para mostrar el código y las habilidades utilizadas en el desarrollo del juego de naves espaciales.

Si tienes interés en colaborar o utilizar parte del código para tus proyectos personales, por favor contáctame para obtener el permiso correspondiente.

¡Gracias por tu comprensión y apoyo!

## Miembros

ijmgithub@gmail.com
ivan.jaraiz@gmail.com


## GitHub

[Visita el repositorio de GitHub](https://github.com/ijmGithub)

## Otros proyectos

- [Jungle Adventure](https://github.com/ijmGithub/selvaAdventureGame): la emocionante aventura en la selva, donde te enfrentarás a desafíos y peligros en cada paso. Explora un mundo misterioso lleno de 
criaturas exóticas, tesoros ocultos y emocionantes acertijos.
- [Space Invaders](https://github.com/ijmGithub/SpaceInvadersGame): Space Invaders es un emocionante juego de naves espaciales inspirado en los clásicos arcade. Únete a la batalla en el espacio exterior y 
defiende la Tierra de la invasión alienígena.
