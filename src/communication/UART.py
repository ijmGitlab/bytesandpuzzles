# UART.py
import serial
import os
import sys
import win32pipe, win32file

def connect_to_uart(port='COM3', baud_rate=9600):
    ser = serial.Serial(port, baud_rate)
    return ser
def send_data(ser, data):
    if ser.isOpen():
        ser.write(data.encode())
    else:
        print("UART connection is not open.")
def receive_data(ser):
    if ser.isOpen():
        return ser.readline().decode().strip()
    else:
        print("UART connection is not open.")
def ipc_server(pipe_name):
    pipe = win32pipe.CreateNamedPipe(
        r'\\.\pipe\{}'.format(pipe_name),
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
        1, 65536, 65536,
        0,
        None)

    win32pipe.ConnectNamedPipe(pipe, None)

    ser = connect_to_uart()

    while True:
        command = win32file.ReadFile(pipe, 64 * 1024)

        if command == b'receive':
            data = receive_data(ser)
            win32file.WriteFile(pipe, data.encode())
        elif command.startswith(b'send:'):
            data = command.split(b':', 1)[1]
            send_data(ser, data.decode())

if __name__ == '__main__':
    pipe_name = sys.argv[1] if len(sys.argv) > 1 else 'UARTPipe'
    ipc_server(pipe_name)