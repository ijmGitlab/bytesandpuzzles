// Character.cs
namespace JuegoDeComunicaciones.BusinessLayer.Models
{
    public class Character
    {
        // Propiedades de personaje, como nombre, salud, nivel, etc.
        public string Name { get; set; }
        // más propiedades...
    }
}

