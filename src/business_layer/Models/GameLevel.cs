// GameLevel.cs
namespace JuegoDeComunicaciones.BusinessLayer.Models
{
    public class GameLevel
    {
        // Propiedades de tu nivel de juego, como dificultad, número de nivel, etc.
        public int Difficulty { get; set; }
        // más propiedades...
    }
}

