// GameState.cs
namespace JuegoDeComunicaciones.BusinessLayer.Models
{
    public class GameState
    {
        // Propiedades del estado del juego, como el personaje actual, el nivel actual, etc.
        public Character CurrentCharacter { get; set; }
        public GameLevel CurrentLevel { get; set; }
        // más propiedades...
    }
}