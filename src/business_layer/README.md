## Estructura de carpeta business_layer

Las rutas exactas pueden variar según la versión y la organización del proyecto. 
A continuación, se muestra la disposición de carpetas para business_layer: 

- `/business_layer` : Código C# para la capa de negocio
  - `/Models` : Clases que representan a los diferentes objetos del juego
    - `Character.cs` : Clase que representa a un personaje del juego
    - `GameLevel.cs` : Clase que representa un nivel del juego
    - `GameState.cs` : Clase que representa el estado actual del juego
  - `/Services` : Clases que encapsulan la lógica de negocio y operan sobre los modelos
      - `GameService.cs` : Clase que maneja la lógica de juego principal
  - `BusinessLogic.cs` : Archivo de código para la capa de negocio
