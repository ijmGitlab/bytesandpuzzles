// BusinessLogic.cs
using JuegoDeComunicaciones.BusinessLayer.Services;

namespace JuegoDeComunicaciones.BusinessLayer
{
    public class BusinessLogic
    {
        private GameService _gameService;

        public BusinessLogic()
        {
            _gameService = new GameService();
        }

        public void StartNewGame()
        {
            _gameService.StartNewGame();
        }

        public GameState GetGameState()
        {
            return _gameService.GetGameState();
        }

        // Más métodos que expongan la lógica de juego a la capa de presentación...
    }
}

