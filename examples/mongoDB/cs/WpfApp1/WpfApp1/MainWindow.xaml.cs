﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MongoDB.Bson;
using MongoDB.Driver;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Configura la cadena de conexión
            var connectionString = "mongodb://localhost:27017";
            var client = new MongoClient(connectionString);

            // Conéctate a la base de datos y la colección
            var database = client.GetDatabase("testDB");
            var collection = database.GetCollection<BsonDocument>("testCollection");

            // Inserta un nuevo documento en la colección
            var document = new BsonDocument
            {
                { "name", "John Doe" },
                { "age", 28 },
                { "address", new BsonDocument
                    {
                        { "street", "123 Main St" },
                        { "city", "Anytown" }
                    }
                }
            };
            collection.InsertOne(document);

            // Realiza una consulta y muestra los resultados
            var filter = Builders<BsonDocument>.Filter.Eq("name", "John Doe");
            var result = collection.Find(filter).FirstOrDefault();
            Console.WriteLine(result.ToString());
        }
    }
}
