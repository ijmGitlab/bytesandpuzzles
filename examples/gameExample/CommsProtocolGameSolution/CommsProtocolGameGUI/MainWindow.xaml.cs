﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CommsProtocolGameGUI
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AudioPlayer.Play(); // Iniciar la reproducción del audio cuando la ventana se carga.
        }
        private void AudioPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            AudioPlayer.Position = TimeSpan.Zero;  // Reinicia la posición del audio.
            AudioPlayer.Play();  // Comienza la reproducción desde el priShowIntroncipio.
        }
        private void ShowIntro(object sender, RoutedEventArgs e)
        {
            IntroPanel.Visibility = Visibility.Visible;
        }
        private void StartGame(object sender, RoutedEventArgs e)
        {
            IntroPanel.Visibility = Visibility.Collapsed;
            // Aquí puedes añadir cualquier lógica adicional que desees ejecutar cuando el juego comience.
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender == txtIP && string.IsNullOrWhiteSpace(txtIP.Text))
                placeholderIP.Visibility = Visibility.Collapsed;
            if (sender == txtPort && string.IsNullOrWhiteSpace(txtPort.Text))
                placeholderPort.Visibility = Visibility.Collapsed;
        }

        private void ConjurarPortal_Click(object sender, RoutedEventArgs e)
        {
            // Aquí, asumiendo que la lógica de conectar (Conjurar Portal) fue exitosa:

            AltarAncestral.Visibility = Visibility.Collapsed;
            MagoIntroPanel.Visibility = Visibility.Visible;
        }

        private void ContinueAfterMagoIntro(object sender, RoutedEventArgs e)
        {
            MagoIntroPanel.Visibility = Visibility.Collapsed;
        }

        private void SendCommandToSTM32(object sender, RoutedEventArgs e)
        {
            AltarDelCristal.Visibility = Visibility.Collapsed;
            PortalOculto.Visibility = Visibility.Visible;
        }
       
    }
}
