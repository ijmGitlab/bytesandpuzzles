﻿using CommsProtocolGameDataLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Newtonsoft.Json;


namespace CommsProtocolGameGUI.ViewModels
{
    public class MainWindowViewModel : NotifyPropertyChangedBase
    {
        #region altarAncestral

        private TcpClient _client = new TcpClient();
        private string _ip;
        private string _puerto;
        private string _connectionStatus;
        private string _crystalResponse;
        private bool _isPortalConjured = false;
        private string _i2cAddress;
        private string _i2cValue;

        public string I2CAddress
        {
            get => _i2cAddress;
            set
            {
                _i2cAddress = value;
                OnPropertyChanged(nameof(I2CAddress));
            }
        }
        public string I2CValue
        {
            get => _i2cValue;
            set
            {
                _i2cValue = value;
                OnPropertyChanged(nameof(I2CValue));
            }
        }

        public string IP
        {
            get => _ip;
            set
            {
                _ip = value;
                OnPropertyChanged(nameof(IP));
            }
        }

        public string Puerto
        {
            get => _puerto;
            set
            {
                _puerto = value;
                OnPropertyChanged(nameof(Puerto));
            }
        }

        public string ConnectionStatus
        {
            get => _connectionStatus;
            set
            {
                _connectionStatus = value;
                OnPropertyChanged(nameof(ConnectionStatus));
            }
        }

        public string CrystalResponse
        {
            get => _crystalResponse;
            set
            {
                _crystalResponse = value;
                OnPropertyChanged(nameof(CrystalResponse));
            }
        }

        public ICommand ConectarCommand { get; set; }
        public ICommand ContinueCommand { get; set; }
        public ICommand SendI2cCommand { get; set; }
        public ICommand EnviarAStmCommand { get; set; }

        private void SendI2c()
        {}

        private void EnviarAStm()
        { }

        private void Conectar()
        {
            // Lógica para intentar conectar usando IP y Puerto.
            ConnectToRaspberry();
            SendCommandToRaspberry(GameDbOperations.Instance.GetCommandByName("invocar_portal"));

            _isPortalConjured = true;
            AreUartL1ChallengesCompleted = false;
            AreUartL2ChallengesCompleted = false;
            AreSpiL1ChallengesCompleted = false;
            AreI2cL1ChallengesCompleted = false;
            I2CChallenges.Clear();
            SPIChallenges.Clear();
            UARTChallenges.Clear();
        }

        private void Continue()
        {
            I2CChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío del Báculo Místico"));
            I2CChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío de la Llave Arcana"));
            I2CChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío del Pergamino Silente"));

            SPIChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío del Escudo Reflector"));
            SPIChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío de la Gema Transmitente"));
            SPIChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío de las Runas Sincrónicas"));

            UARTChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío del Guardián de Baudios"));
            UARTChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío de la Torre Paridad"));
            UARTChallenges.Add(GameDbOperations.Instance.GetChallengeByName("Desafío del Eco del Pasado"));
        }

        private void ConnectToRaspberry()
        {
            try
            {
                _client.Connect(IP, int.Parse(Puerto));
            }
            catch (Exception ex)
            {
                // Aquí puedes manejar el error específico, por ahora lo mostraremos directamente.
                MessageBox.Show($"¡Oh, aventurero! Parece que las energías místicas nos están jugando una mala pasada. No podemos establecer un puente mágico con el reino de la Raspberry Pi. Asegúrate de que el portal (Raspberry Pi) esté activo y listo para conectarse. Detalles místicos del error: {ex.Message}", "Error de Conjuro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CloseConnectionToRaspberry()
        {
            try
            {
                if (_client != null && _client.Connected)
                {
                    _client.Close();
                }
            }
            catch (Exception ex)
            {
                // Aquí puedes manejar el error específico, por ahora lo mostraremos directamente.
                MessageBox.Show($"¡Oh, aventurero! Parece que las energías místicas nos están jugando una mala pasada. No podemos establecer un puente mágico con el reino de la Raspberry Pi. Asegúrate de que el portal (Raspberry Pi) esté activo y listo para conectarse. Detalles místicos del error: {ex.Message}", "Error de Conjuro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SendCommandToRaspberry(CommandData command)
        {
            if (!_client.Connected)
            {
                ConnectToRaspberry();
            }

            string dataToSend = JsonConvert.SerializeObject(command);  // Convertimos el comando a formato JSON

            try
            {
                using (var stream = _client.GetStream())
                {
                    byte[] dataBytes = Encoding.UTF8.GetBytes(dataToSend);
                    stream.Write(dataBytes, 0, dataBytes.Length);
                }
            }
            catch (Exception ex)
            {
                // Aquí puedes manejar el error específico, por ahora lo mostraremos directamente.
                MessageBox.Show($"¡Oh, aventurero! Parece que las energías místicas nos están jugando una mala pasada. No podemos establecer un puente mágico con el reino de la Raspberry Pi. Asegúrate de que el portal (Raspberry Pi) esté activo y listo para conectarse. Detalles místicos del error: {ex.Message}", "Error de Conjuro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion altarAncestral

        private bool _areUartL1ChallengesCompleted;
        public bool AreUartL1ChallengesCompleted
        {
            get { return _areUartL1ChallengesCompleted; }
            set
            {
                _areUartL1ChallengesCompleted = value;
                OnPropertyChanged(nameof(AreUartL1ChallengesCompleted));
            }
        }

        private bool _areUartL2ChallengesCompleted;
        public bool AreUartL2ChallengesCompleted
        {
            get { return _areUartL2ChallengesCompleted; }
            set
            {
                _areUartL2ChallengesCompleted = value;
                OnPropertyChanged(nameof(AreUartL2ChallengesCompleted));
            }
        }

        private bool _areSpiL1ChallengesCompleted;
        public bool AreSpiL1ChallengesCompleted
        {
            get { return _areSpiL1ChallengesCompleted; }
            set
            {
                _areSpiL1ChallengesCompleted = value;
                OnPropertyChanged(nameof(AreSpiL1ChallengesCompleted));
            }
        }

        private bool _areSpiL2ChallengesCompleted;
        public bool AreSpiL2ChallengesCompleted
        {
            get { return _areSpiL2ChallengesCompleted; }
            set
            {
                _areSpiL2ChallengesCompleted = value;
                OnPropertyChanged(nameof(AreSpiL2ChallengesCompleted));
            }
        }

        private bool _areI2cL1ChallengesCompleted;
        public bool AreI2cL1ChallengesCompleted
        {
            get { return _areI2cL1ChallengesCompleted; }
            set
            {
                _areI2cL1ChallengesCompleted = value;
                OnPropertyChanged(nameof(AreI2cL1ChallengesCompleted));
            }
        }

        private bool _areI2cL2ChallengesCompleted;
        public bool AreI2cL2ChallengesCompleted
        {
            get { return _areI2cL2ChallengesCompleted; }
            set
            {
                _areI2cL2ChallengesCompleted = value;
                OnPropertyChanged(nameof(AreI2cL2ChallengesCompleted));
            }
        }

        // 1. Colecciones para cada protocolo
        private ObservableCollection<ProtocolChallenge> _challenges;

        private ObservableCollection<ProtocolChallenge> _i2cChallenges;
        private ObservableCollection<ProtocolChallenge> _spiChallenges;
        private ObservableCollection<ProtocolChallenge> _uartChallenges;


        // 2. Desafíos seleccionados para cada protocolo
        private ProtocolChallenge _selectedChallenge;

        private ProtocolChallenge _selectedI2CChallenge;
        private ProtocolChallenge _selectedSPIChallenge;
        private ProtocolChallenge _selectedUARTChallenge;

        private int _selectedOptionIndex = -1; // por defecto, ninguna opción seleccionada.

        // 3. Score para cada protocolo
        private int _score;

        private int _scoreI2C;
        private int _scoreSPI;
        private int _scoreUART;

        // 4. Feedback para cada protocolo
        private string _feedback;

        private string _feedbackI2C;
        private string _feedbackSPI;
        private string _feedbackUART;

        // 5. Comandos para cada protocolo
        public ICommand SubmitAnswerCommand { get; set; }

        public ICommand SubmitAnswerI2CCommand { get; set; }
        public ICommand SubmitAnswerSPICommand { get; set; }
        public ICommand SubmitAnswerUARTCommand { get; set; }


        private ProtocolType _selectedProtocolType; // Nuevo: Para seleccionar el protocolo
 
        public ObservableCollection<ProtocolChallenge> Challenges
        {
            get { return _challenges; }
            set
            {
                _challenges = value;
                OnPropertyChanged(nameof(Challenges));
            }
        }

        public ObservableCollection<ProtocolChallenge> I2CChallenges
        {
            get { return _i2cChallenges; }
            set
            {
                _i2cChallenges = value;
                OnPropertyChanged(nameof(I2CChallenges));
            }
        }

        public ObservableCollection<ProtocolChallenge> SPIChallenges
        {
            get { return _spiChallenges; }
            set
            {
                _spiChallenges = value;
                OnPropertyChanged(nameof(SPIChallenges));
            }
        }

        public ObservableCollection<ProtocolChallenge> UARTChallenges
        {
            get { return _uartChallenges; }
            set
            {
                _uartChallenges = value;
                OnPropertyChanged(nameof(UARTChallenges));
            }
        }

        public ProtocolChallenge SelectedChallenge
        {
            get { return _selectedChallenge; }
            set
            {
                _selectedChallenge = value;
                OnPropertyChanged(nameof(SelectedChallenge));
            }
        }

        public ProtocolChallenge SelectedI2CChallenge
        {
            get { return _selectedI2CChallenge; }
            set
            {
                _selectedI2CChallenge = value;
                OnPropertyChanged(nameof(SelectedI2CChallenge));
            }
        }

        public ProtocolChallenge SelectedSPIChallenge
        {
            get { return _selectedSPIChallenge; }
            set
            {
                _selectedSPIChallenge = value;
                OnPropertyChanged(nameof(SelectedSPIChallenge));
            }
        }

        public ProtocolChallenge SelectedUARTChallenge
        {
            get { return _selectedUARTChallenge; }
            set
            {
                _selectedUARTChallenge = value;
                OnPropertyChanged(nameof(SelectedUARTChallenge));
            }
        }

        public int SelectedOptionIndex
        {
            get { return _selectedOptionIndex; }
            set
            {
                if (_selectedOptionIndex != value)
                {
                    _selectedOptionIndex = value;
                    OnPropertyChanged(nameof(SelectedOptionIndex));
                }
            }
        }

        public string Feedback
        {
            get { return _feedback; }
            set
            {
                _feedback = value;
                OnPropertyChanged(nameof(Feedback));
            }
        }

        public string FeedbackI2C
        {
            get { return _feedbackI2C; }
            set
            {
                _feedbackI2C = value;
                OnPropertyChanged(nameof(FeedbackI2C));
            }
        }

        public string FeedbackSPI
        {
            get { return _feedbackSPI; }
            set
            {
                _feedbackSPI = value;
                OnPropertyChanged(nameof(FeedbackSPI));
            }
        }

        public string FeedbackUART
        {
            get { return _feedbackUART; }
            set
            {
                _feedbackUART = value;
                OnPropertyChanged(nameof(FeedbackUART));
            }
        }

        public int Score
        {
            get { return _score; }
            set
            {
                _score = value;
                OnPropertyChanged(nameof(Score));
            }
        }

        public int ScoreI2C
        {
            get { return _scoreI2C; }
            set
            {
                _scoreI2C = value;
                OnPropertyChanged(nameof(ScoreI2C));
            }
        }

        public int ScoreSPI
        {
            get { return _scoreSPI; }
            set
            {
                _scoreSPI = value;
                OnPropertyChanged(nameof(ScoreSPI));
            }
        }

        public int ScoreUART
        {
            get { return _scoreUART; }
            set
            {
                _scoreUART = value;
                OnPropertyChanged(nameof(ScoreUART));
            }
        }

        public ProtocolType SelectedProtocolType
        {
            get { return _selectedProtocolType; }
            set
            {
                _selectedProtocolType = value;
                OnPropertyChanged(nameof(SelectedProtocolType));
            }
        }

        public ICommand InitializeGameCommand { get; set; }


        public MainWindowViewModel()
        {
            Challenges = new ObservableCollection<ProtocolChallenge>();
            SPIChallenges = new ObservableCollection<ProtocolChallenge>();
            I2CChallenges = new ObservableCollection<ProtocolChallenge>();
            UARTChallenges = new ObservableCollection<ProtocolChallenge>();
            InitializeGameCommand = new RelayCommand(obj => InitializeGame());
            SubmitAnswerCommand = new RelayCommand(obj => SubmitAnswer());
            SubmitAnswerUARTCommand = new RelayCommand(obj => SubmitUARTAnswer());
            SubmitAnswerI2CCommand = new RelayCommand(obj => SubmitI2CAnswer());
            SubmitAnswerSPICommand = new RelayCommand(obj => SubmitSPIAnswer());

            // Inicializar otras propiedades y comandos
            ConectarCommand = new RelayCommand(obj => Conectar());
            ContinueCommand = new RelayCommand(obj => Continue());
            SendI2cCommand = new RelayCommand(obj => SendI2c());
            EnviarAStmCommand = new RelayCommand(obj => EnviarAStm());
        }

        private void InitializeGame()
        {
            GameDbOperations.Instance.DeleteAllDocumentsFromCollection();
            InitializeSpiChallenges(GameDbOperations.Instance);
            InitializeI2CChallenges(GameDbOperations.Instance);
            InitializeUARTChallenges(GameDbOperations.Instance);
            InitializeUARTChallenges(GameDbOperations.Instance);
            InitializeCommand(GameDbOperations.Instance);
            GameDbOperations.Instance.ResetAdventure();
        }

        private void InitializeCommand(GameDbOperations dbOperations)
        {

            var invocarPortalCommand = new CommandData
            {
                Command = "invocar_portal",
                Parameters = null,  // No hay parámetros para este comando específico
                Timestamp = DateTime.UtcNow
            };

            // Insertando comandos en la base de datos
            dbOperations.SaveCommandToDb(invocarPortalCommand);
        }


        private void InitializeSpiChallenges(GameDbOperations dbOperations)
        {
            // Desafíos SPI (Reino Tecnológico Avanzado)

            var challengeMagoBuffer = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Mago Tecnológico del Buffer",
                ProtocolType = GameProtocolType.SPI_L1,
                Command = "ALLOCATE_BUFFER_MEMORY",
                ExpectedResponse = "BUFFER_CREATED",
                Hint = "En el Reino Tecnológico Avanzado, usa tu magia cibernética para crear un espacio donde las secuencias de datos puedan ser almacenadas de manera segura.",
                RewardPoints = 40,
                ResponseOptions = new List<string> { "BUFFER_CREATED", "INSUFFICIENT_MEMORY", "BUFFER_OVERFLOW", "FAILED_REQUEST" },
                IsCompleted = false
            };

            var challengeGuerreroSincronizacion = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Guerrero de la Red Tecnológica",
                ProtocolType = GameProtocolType.SPI_L1,
                Command = "SYNC_SLAVE_DEVICES",
                ExpectedResponse = "SYNC_SUCCESS",
                Hint = "En este mundo de circuitos y conexiones, como un valiente guerrero tecnológico, asegura la sincronización perfecta entre todos los dispositivos.",
                RewardPoints = 45,
                ResponseOptions = new List<string> { "SYNC_SUCCESS", "SLAVE_MISSING", "TIMING_ERROR", "UNKNOWN_ERROR" },
                IsCompleted = false
            };

            var challengeDruidaControlador = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Druida de la Matrix Digital",
                ProtocolType = GameProtocolType.SPI_L1,
                Command = "INIT_SPI_CONTROLLER",
                ExpectedResponse = "INIT_SUCCESS",
                Hint = "Desde las profundidades de la red tecnológica, invoca el poder de los circuitos para inicializar el controlador SPI central.",
                RewardPoints = 50,
                ResponseOptions = new List<string> { "INIT_SUCCESS", "ALREADY_INITIALIZED", "HARDWARE_ERROR", "DIGITAL_MALFUNCTION" },
                IsCompleted = false
            };

            var challengeArqueroInterrupcion = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Arquero de las Señales Eléctricas",
                ProtocolType = GameProtocolType.SPI_L1,
                Command = "SET_INTERRUPT_HANDLERS",
                ExpectedResponse = "HANDLER_SET",
                Hint = "Como un arquero en un campo de batalla digital, ajusta las señales eléctricas y configura los manejadores de interrupción con precisión milimétrica.",
                RewardPoints = 55,
                ResponseOptions = new List<string> { "HANDLER_SET", "HANDLER_MISSING", "DUPLICATE_HANDLER", "SIGNAL_ERROR" },
                IsCompleted = false
            };

            var challengeAlquimistaConfiguracion = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Alquimista de la Configuración Binaria",
                ProtocolType = GameProtocolType.SPI_L1,
                Command = "CONFIGURE_SPI_MODE",
                ExpectedResponse = "MODE_CONFIGURED",
                Hint = "Alquimista de bytes y bits, es tu labor mezclar las configuraciones adecuadas para alcanzar la armonía en el protocolo SPI.",
                RewardPoints = 60,
                ResponseOptions = new List<string> { "MODE_CONFIGURED", "INVALID_MODE", "CONFIG_CONFLICT", "INVALID_ALGORITHM" },
                IsCompleted = false
            };


            // Desafios Mago Raspberry Pi. 

            // Este desafío puede estar relacionado con la configuración del modo SPI (por ejemplo, establecer el modo correcto según el flanco y la fase del reloj).
            var challengeEscudoReflector = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Escudo Reflector",
                ProtocolType = GameProtocolType.SPI_L2,
                Command = "SET_SHIELD_MODE",
                ExpectedResponse = "SHIELD_MODE_SET",
                Hint = "En el Reino Tecnológico Avanzado, el Escudo Reflector puede proteger o bloquear información. Configura correctamente el modo SPI para que la comunicación fluya sin interferencias.",
                RewardPoints = 45,
                ResponseOptions = new List<string> { "SHIELD_MODE_SET", "MODE_MISMATCH", "SHIELD_UNRESPONSIVE", "FAILED_REQUEST" },
                IsCompleted = false
            };

            // La idea de este desafío es enviar un paquete específico de datos a través de SPI y obtener una respuesta.
            var challengeGemaTransmitente = new ProtocolChallenge
            {
                ChallengeName = "Desafío de la Gema Transmitente",
                ProtocolType = GameProtocolType.SPI_L2,
                Command = "SEND_GEM_SIGNAL",
                ExpectedResponse = "GEM_RESPONSE_RECEIVED",
                Hint = "La Gema Transmitente emite una señal única. Envía la secuencia mágica a través de SPI y espera la respuesta luminosa de la gema.",
                RewardPoints = 60,
                ResponseOptions = new List<string> { "GEM_RESPONSE_RECEIVED", "GEM_UNRESPONSIVE", "GEM_DISTORTED", "FAILED_REQUEST" },
                IsCompleted = false
            };

            // En este desafío, el jugador podría necesitar configurar la velocidad (baud rate) del SPI para que coincida con una tasa específica.
            var challengeRunasSincronicas = new ProtocolChallenge
            {
                ChallengeName = "Desafío de las Runas Sincrónicas",
                ProtocolType = GameProtocolType.SPI_L2,
                Command = "SET_RUNE_RATE",
                ExpectedResponse = "RUNE_RATE_MATCHED",
                Hint = "Las Runas Sincrónicas requieren una sintonización fina. Ajusta la velocidad de SPI para comunicarte a la tasa de las runas y descubre su mensaje oculto.",
                RewardPoints = 55,
                ResponseOptions = new List<string> { "RUNE_RATE_MATCHED", "RATE_MISMATCH", "RUNE_UNREADABLE", "FAILED_REQUEST" },
                IsCompleted = false
            };

            // Insertando desafíos en la base de datos
            dbOperations.InsertProtocolChallenge(challengeMagoBuffer);
            dbOperations.InsertProtocolChallenge(challengeGuerreroSincronizacion);
            dbOperations.InsertProtocolChallenge(challengeDruidaControlador);
            dbOperations.InsertProtocolChallenge(challengeArqueroInterrupcion);
            dbOperations.InsertProtocolChallenge(challengeAlquimistaConfiguracion);
            dbOperations.InsertProtocolChallenge(challengeEscudoReflector);
            dbOperations.InsertProtocolChallenge(challengeGemaTransmitente);
            dbOperations.InsertProtocolChallenge(challengeRunasSincronicas);

            // Suponiendo que queremos mostrar estos desafíos en la interfaz de usuario:
            SPIChallenges.Clear();
            SPIChallenges.Add(challengeMagoBuffer);
            SPIChallenges.Add(challengeGuerreroSincronizacion);
            SPIChallenges.Add(challengeDruidaControlador);
            SPIChallenges.Add(challengeArqueroInterrupcion);
            SPIChallenges.Add(challengeAlquimistaConfiguracion);
        }

        private void InitializeI2CChallenges(GameDbOperations dbOperations)
        {
            // Desafíos I2C (Reino Místico)

            var challengeOraculoBus = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Oráculo del Bus",
                ProtocolType = GameProtocolType.I2C_L1,
                Command = "INIT_BUS_ORACLE",
                ExpectedResponse = "ORACLE_READY",
                Hint = "En las tierras del Reino Místico, el Oráculo controla el flujo de información. Invócalo y establece el bus I2C.",
                RewardPoints = 70,
                ResponseOptions = new List<string> { "ORACLE_READY", "ORACLE_BUSY", "ORACLE_ERROR", "INCANTATION_FAILED" },
                IsCompleted = false
            };

            var challengeGuardianDirecciones = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Guardián de las Direcciones",
                ProtocolType = GameProtocolType.I2C_L1,
                Command = "SET_SLAVE_ADDRESS",
                ExpectedResponse = "ADDRESS_SET",
                Hint = "Cada entidad en el Reino Místico tiene una dirección única. Como guardián, asigna una dirección al dispositivo esclavo.",
                RewardPoints = 75,
                ResponseOptions = new List<string> { "ADDRESS_SET", "ADDRESS_CONFLICT", "INVALID_ADDRESS", "GUARDIAN_REJECTION" },
                IsCompleted = false
            };

            var challengeHechiceroDatos = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Hechicero de los Datos",
                ProtocolType = GameProtocolType.I2C_L1,
                Command = "TRANSFER_DATA_SPELL",
                ExpectedResponse = "DATA_TRANSFERRED",
                Hint = "Utilizando encantamientos antiguos, transfiere los datos místicos a través del bus I2C.",
                RewardPoints = 80,
                ResponseOptions = new List<string> { "DATA_TRANSFERRED", "SPELL_FAILED", "INCORRECT_DATA", "HECHICERO_BUSY" },
                IsCompleted = false
            };

            var challengeCaballeroClock = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Caballero del Reloj",
                ProtocolType = GameProtocolType.I2C_L1,
                Command = "SET_CLOCK_RATE",
                ExpectedResponse = "RATE_SET",
                Hint = "Como Caballero del Reloj, ajusta la velocidad de la comunicación mística en el bus I2C para que todos se muevan al ritmo correcto.",
                RewardPoints = 85,
                ResponseOptions = new List<string> { "RATE_SET", "RATE_TOO_HIGH", "RATE_TOO_LOW", "KNIGHT_DISAGREES" },
                IsCompleted = false
            };

            var challengeSacerdotisaConexion = new ProtocolChallenge
            {
                ChallengeName = "Desafío de la Sacerdotisa de la Conexión",
                ProtocolType = GameProtocolType.I2C_L1,
                Command = "ENSURE_STABLE_CONNECTION",
                ExpectedResponse = "CONNECTION_STABLE",
                Hint = "La Sacerdotisa protege las conexiones en el Reino Místico. Asegúrate de que el bus I2C tenga una conexión estable y sin interrupciones.",
                RewardPoints = 90,
                ResponseOptions = new List<string> { "CONNECTION_STABLE", "CONNECTION_LOST", "MAGICAL_INTERFERENCE", "SACERDOTISA_BUSY" },
                IsCompleted = false
            };


            // Desafíos I2C (Mago Raspberry Pi)

            // Este desafío puede estar relacionado con la configuración de la dirección del I2C.
            var challengeBaculoMistico = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Báculo Místico",
                ProtocolType = GameProtocolType.I2C_L2,
                Command = "SET_WAND_ADDRESS",
                ExpectedResponse = "WAND_ADDRESS_SET",
                Hint = "El Báculo Místico necesita una dirección para canalizar sus poderes. Establece la dirección I2C del Báculo.",
                RewardPoints = 50,
                ResponseOptions = new List<string> { "WAND_ADDRESS_SET", "WAND_OVERLOAD", "WAND_MISCONFIGURED", "INCANTATION_FAILED" },
                IsCompleted = false
            };

            // Este desafío puede estar relacionado con la configuración de la dirección del I2C.
            var challengeLlaveArcana = new ProtocolChallenge
            {
                ChallengeName = "Desafío de la Llave Arcana",
                ProtocolType = GameProtocolType.I2C_L2,
                Command = "SEND_ARCANE_KEY",
                ExpectedResponse = "ARCANE_MESSAGE_UNLOCKED",
                Hint = "El STM-F303K8 guarda un mensaje místico. Envia la clave correcta a través de I2C para desbloquearlo.",
                RewardPoints = 80,
                ResponseOptions = new List<string> { "ARCANE_MESSAGE_UNLOCKED", "KEY_MISMATCH", "KEY_CORRUPTED", "INCANTATION_FAILED" },
                IsCompleted = false
            };

            // En este desafío, la idea es recibir un mensaje del STM-F303K8 sin enviar ningún comando previo.
            var challengePergaminoSilente = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Pergamino Silente",
                ProtocolType = GameProtocolType.I2C_L2,
                Command = "REQUEST_SILENT_SCROLL",
                ExpectedResponse = "SILENT_MESSAGE_RECEIVED",
                Hint = "Hay un pergamino que transmite sin ser solicitado. Inicia la escucha en I2C y espera el mensaje del Pergamino Silente.",
                RewardPoints = 60,
                ResponseOptions = new List<string> { "SILENT_MESSAGE_RECEIVED", "SILENT_SCROLL_EMPTY", "SCROLL_CORRUPTED", "INCANTATION_FAILED" },
                IsCompleted = false
            };

            // Insertando desafíos en la base de datos
            dbOperations.InsertProtocolChallenge(challengeOraculoBus);
            dbOperations.InsertProtocolChallenge(challengeGuardianDirecciones);
            dbOperations.InsertProtocolChallenge(challengeHechiceroDatos);
            dbOperations.InsertProtocolChallenge(challengeCaballeroClock);
            dbOperations.InsertProtocolChallenge(challengeSacerdotisaConexion);

            dbOperations.InsertProtocolChallenge(challengeBaculoMistico);
            dbOperations.InsertProtocolChallenge(challengeLlaveArcana);
            dbOperations.InsertProtocolChallenge(challengePergaminoSilente);

            // Suponiendo que queremos mostrar estos desafíos en la interfaz de usuario:
            I2CChallenges.Clear();
            I2CChallenges.Add(challengeOraculoBus);
            I2CChallenges.Add(challengeGuardianDirecciones);
            I2CChallenges.Add(challengeHechiceroDatos);
            I2CChallenges.Add(challengeCaballeroClock);
            I2CChallenges.Add(challengeSacerdotisaConexion);
        }

        private void InitializeUARTChallenges(GameDbOperations dbOperations) 
        {
            // Desafíos UART (Reino Antiguo)

            var challengeEscribaTransmision  = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Escriba de la Transmisión",
                ProtocolType = GameProtocolType.UART_L1,
                Command = "INIT_TRANSMISSION_SCROLL",
                ExpectedResponse = "SCROLL_OPENED",
                Hint = "En tiempos antiguos, los escribas guardaban la sabiduría. Inicia una transmisión escribiendo en el pergamino sagrado (buffer) de UART.",
                RewardPoints = 100,
                ResponseOptions = new List<string> { "SCROLL_OPENED", "SCROLL_BUSY", "INK_SPILLED", "SCROLL_TORN" },
                IsCompleted = false
            };

            var challengeHeraldoBaudios = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Heraldo de los Baudios",
                ProtocolType = GameProtocolType.UART_L1,
                Command = "SET_BAUD_RATE",
                ExpectedResponse = "BAUD_PROCLAIMED",
                Hint = "Como un heraldo que anuncia proclamas, define la velocidad (baud rate) a la que se comunicarán las antiguas entidades del Reino UART.",
                RewardPoints = 105,
                ResponseOptions = new List<string> { "BAUD_PROCLAIMED", "BAUD_TOO_LOUD", "BAUD_WHISPERED", "HERALD_ABSENT" },
                IsCompleted = false
            };

            var challengeGuardianParidad = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Guardián de la Paridad",
                ProtocolType = GameProtocolType.UART_L1,
                Command = "SET_PARITY",
                ExpectedResponse = "PARITY_GUARDED",
                Hint = "Asegúrate de que los mensajes del Reino Antiguo estén balanceados y sin corrupción estableciendo correctamente la paridad.",
                RewardPoints = 110,
                ResponseOptions = new List<string> { "PARITY_GUARDED", "PARITY_BROKEN", "GUARDIAN_ASLEEP", "WRONG_PARITY_CHOSEN" },
                IsCompleted = false
            };

            var challengeMaestroBits = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Maestro de los Bits",
                ProtocolType = GameProtocolType.UART_L1,
                Command = "SET_DATA_BITS",
                ExpectedResponse = "BITS_MASTERY_SHOWN",
                Hint = "Con maestría y precisión antigua, define cuántos bits de datos serán enviados en cada mensaje del Reino UART.",
                RewardPoints = 115,
                ResponseOptions = new List<string> { "BITS_MASTERY_SHOWN", "TOO_MANY_BITS", "NOT_ENOUGH_BITS", "MAESTRO_CONFUSED" },
                IsCompleted = false
            };

            var challengeCustodioStop = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Custodio del Stop",
                ProtocolType = GameProtocolType.UART_L1,
                Command = "SET_STOP_BITS",
                ExpectedResponse = "STOP_SECURED",
                Hint = "Como un custodio que cierra las puertas del castillo, define cuántos bits de parada se utilizarán al final de cada transmisión.",
                RewardPoints = 120,
                ResponseOptions = new List<string> { "STOP_SECURED", "DOOR_STUCK", "GUARD_MISSING", "WRONG_DOOR_CHOSEN" },
                IsCompleted = false
            };

            // Desafios Mago Raspberry Pi

            // Este desafío podría estar relacionado con establecer la velocidad correcta de transmisión (baud rate) para la comunicación UART.
            var challengeGuardianDeBaudios = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Guardián de Baudios",
                ProtocolType = GameProtocolType.UART_L2,
                Command = "SET_BAUD_RATE",
                ExpectedResponse = "BAUD_SET",
                Hint = "En el Reino Antiguo, el Guardián de Baudios mantiene el ritmo de la comunicación. Ajusta correctamente la velocidad de transmisión para ganar su favor.",
                RewardPoints = 110,
                ResponseOptions = new List<string> { "BAUD_SET", "BAUD_MISMATCH", "GUARDIAN_ASLEEP", "FAILED_REQUEST" },
                IsCompleted = false
            };

            // Aquí, el jugador puede necesitar configurar el bit de paridad adecuado para la comunicación UART.
            var challengeTorreParidad = new ProtocolChallenge
            {
                ChallengeName = "Desafío de la Torre Paridad",
                ProtocolType = GameProtocolType.UART_L2,
                Command = "SET_PARITY",
                ExpectedResponse = "PARITY_MATCHED",
                Hint = "La Torre Paridad protege los datos con un encantamiento especial. Ajusta correctamente el bit de paridad para decodificar sus secretos.",
                RewardPoints = 90,
                ResponseOptions = new List<string> { "PARITY_MATCHED", "PARITY_MISMATCH", "TORRE_UNRESPONSIVE", "FAILED_REQUEST" },
                IsCompleted = false
            };

            // En este desafío, el jugador puede enviar un mensaje específico y esperar que sea devuelto exactamente, haciendo eco de la transmisión.
            var challengeEcoDelPasado = new ProtocolChallenge
            {
                ChallengeName = "Desafío del Eco del Pasado",
                ProtocolType = GameProtocolType.UART_L2,
                Command = "SEND_ECHO_MESSAGE",
                ExpectedResponse = "MESSAGE_ECHOED",
                Hint = "Los ecos del pasado resuenan en las paredes del Reino Antiguo. Envía un mensaje al abismo y espera que te sea devuelto intacto.",
                RewardPoints = 95,
                ResponseOptions = new List<string> { "MESSAGE_ECHOED", "ECHO_DISTORTED", "ABYSS_SILENT", "FAILED_REQUEST" },
                IsCompleted = false
            };



            // Insertando desafíos en la base de datos
            dbOperations.InsertProtocolChallenge(challengeEscribaTransmision);
            dbOperations.InsertProtocolChallenge(challengeHeraldoBaudios);
            dbOperations.InsertProtocolChallenge(challengeGuardianParidad);
            dbOperations.InsertProtocolChallenge(challengeMaestroBits);
            dbOperations.InsertProtocolChallenge(challengeCustodioStop);
            dbOperations.InsertProtocolChallenge(challengeGuardianDeBaudios);
            dbOperations.InsertProtocolChallenge(challengeTorreParidad);
            dbOperations.InsertProtocolChallenge(challengeEcoDelPasado);

            // Suponiendo que queremos mostrar estos desafíos en la interfaz de usuario:
            UARTChallenges.Clear();
            UARTChallenges.Add(challengeEscribaTransmision);
            UARTChallenges.Add(challengeHeraldoBaudios);
            UARTChallenges.Add(challengeGuardianParidad);
            UARTChallenges.Add(challengeMaestroBits);
            UARTChallenges.Add(challengeCustodioStop);
        }

        private void SubmitAnswer()
        {
            if (_selectedChallenge == null || _selectedChallenge.IsCompleted) return;

            string userResponse = _selectedChallenge.ResponseOptions[_selectedOptionIndex];

            if (userResponse == _selectedChallenge.ExpectedResponse)
            {
                Score += _selectedChallenge.RewardPoints;
                Feedback = $"¡Correcto! Ganaste {_selectedChallenge.RewardPoints} puntos.";
                _selectedChallenge.IsCompleted = true;  // Marca el desafío como completado
            }
            else
            {
                Feedback = "Respuesta incorrecta. Intenta nuevamente.";
            }
        }

        private void SubmitUARTAnswer()
        {
            try
            {
                if (_selectedUARTChallenge == null || _selectedUARTChallenge.IsCompleted) return;

                // Verifica si alguna opción fue seleccionada
                if (_selectedOptionIndex < 0 || _selectedOptionIndex >= _selectedUARTChallenge.ResponseOptions.Count)
                {
                    throw new InvalidOperationException("Ninguna opción seleccionada");
                }

                string userResponse = _selectedUARTChallenge.ResponseOptions[_selectedOptionIndex];

                if (userResponse == _selectedUARTChallenge.ExpectedResponse)
                {
                    Score += _selectedUARTChallenge.RewardPoints;
                    FeedbackUART = $"¡Correcto! Ganaste {_selectedUARTChallenge.RewardPoints} puntos.";
                    _selectedUARTChallenge.IsCompleted = true;  // Marca el desafío como completado

                    ProtocolChallenge value = GameDbOperations.Instance.GetChallengeByName(_selectedUARTChallenge.ChallengeName);
                    value.IsCompleted = true;
                    GameDbOperations.Instance.UpdateChallenge(value);

                    if (!_isPortalConjured)
                    {
                        // verifica si todos los desafíos UART están completados y, en caso afirmativo, otorga la recompensa:
                        if (GameDbOperations.Instance.AreAllChallengesCompletedForProtocol(GameProtocolType.UART_L1))
                        {
                            AreUartL1ChallengesCompleted = true;
                            MessageBox.Show("Has mostrado respeto y valentía en el Reino Antiguo de UART. Ahora, el altar ancestral se desbloquea para ti.");
                            // En el Altar Ancestral, puedes auto rellenar los campos de IP y Puerto, o dejar que el jugador los ingrese como un rompecabezas.                     
                        }
                    }
                    else
                    {
                        // verifica si todos los desafíos UART están completados y, en caso afirmativo, otorga la recompensa:
                        if (GameDbOperations.Instance.AreAllChallengesCompletedForProtocol(GameProtocolType.UART_L2))
                        {
                            AreUartL2ChallengesCompleted = true;
                            MessageBox.Show("XXXXX");
                        }
                    }
                }
                else
                {
                    FeedbackUART = "Respuesta incorrecta. Intenta nuevamente.";
                }
            }
            catch (InvalidOperationException ex)
            {
                // Mostrar un mensaje de error siguiendo la temática
                MessageBox.Show($"Hechizo fallido: {ex.Message}. Invoca correctamente el espíritu de la respuesta.");
            }
            catch (Exception ex)
            {
                // Para otros errores inesperados
                MessageBox.Show($"Error mágico inesperado: {ex.Message}. Consulta con el Oráculo (soporte técnico).");
            }
        }

        private void SubmitSPIAnswer()
        {
            try
            {
                if (_selectedSPIChallenge == null || _selectedSPIChallenge.IsCompleted) return;

                // Verifica si alguna opción fue seleccionada
                if (_selectedOptionIndex < 0 || _selectedOptionIndex >= _selectedSPIChallenge.ResponseOptions.Count)
                {
                    throw new InvalidOperationException("Ninguna opción seleccionada");
                }

                string userResponse = _selectedSPIChallenge.ResponseOptions[_selectedOptionIndex];

                if (userResponse == _selectedSPIChallenge.ExpectedResponse)
                {
                    Score += _selectedSPIChallenge.RewardPoints;
                    FeedbackSPI = $"¡Correcto! Ganaste {_selectedSPIChallenge.RewardPoints} puntos.";
                    _selectedSPIChallenge.IsCompleted = true;  // Marca el desafío como completado

                    ProtocolChallenge value = GameDbOperations.Instance.GetChallengeByName(_selectedSPIChallenge.ChallengeName);
                    value.IsCompleted = true;
                    GameDbOperations.Instance.UpdateChallenge(value);

                    if (!_isPortalConjured)
                    {
                        // verifica si todos los desafíos SPI están completados y, en caso afirmativo, otorga la recompensa:
                        if (GameDbOperations.Instance.AreAllChallengesCompletedForProtocol(GameProtocolType.SPI_L1))
                        {
                            AreSpiL1ChallengesCompleted = true;
                            int puerto = 5000; // reemplaza con el puerto real
                            MessageBox.Show($"Has demostrado destreza en el Reino Tecnológico de SPI. Concede la Secuencia de Invocación: {puerto}.");
                        }
                    }
                    else
                    {
                        // verifica si todos los desafíos UART están completados y, en caso afirmativo, otorga la recompensa:
                        if (GameDbOperations.Instance.AreAllChallengesCompletedForProtocol(GameProtocolType.SPI_L2))
                        {
                            AreSpiL2ChallengesCompleted = true;
                            MessageBox.Show("XXXXX");
                        }
                    }
                }
                else
                {
                    FeedbackSPI = "Respuesta incorrecta. Intenta nuevamente.";
                }
            }
            catch (InvalidOperationException ex)
            {
                // Mostrar un mensaje de error siguiendo la temática
                MessageBox.Show($"Hechizo fallido: {ex.Message}. Invoca correctamente el espíritu de la respuesta.");
            }
            catch (Exception ex)
            {
                // Para otros errores inesperados
                MessageBox.Show($"Error mágico inesperado: {ex.Message}. Consulta con el Oráculo (soporte técnico).");
            }
        }

        private void SubmitI2CAnswer()
        {
            try
            {
                if (_selectedI2CChallenge == null || _selectedI2CChallenge.IsCompleted) return;

                // Verifica si alguna opción fue seleccionada
                if (_selectedOptionIndex < 0 || _selectedOptionIndex >= _selectedI2CChallenge.ResponseOptions.Count)
                {
                    throw new InvalidOperationException("Ninguna opción seleccionada");
                }

                string userResponse = _selectedI2CChallenge.ResponseOptions[_selectedOptionIndex];

                if (userResponse == _selectedI2CChallenge.ExpectedResponse)
                {
                    Score += _selectedI2CChallenge.RewardPoints;
                    FeedbackI2C = $"¡Correcto! Ganaste {_selectedI2CChallenge.RewardPoints} puntos.";
                    _selectedI2CChallenge.IsCompleted = true;  // Marca el desafío como completado

                    ProtocolChallenge value = GameDbOperations.Instance.GetChallengeByName(_selectedI2CChallenge.ChallengeName);
                    value.IsCompleted = true;
                    GameDbOperations.Instance.UpdateChallenge(value);

                    if (!_isPortalConjured)
                    {
                        // verifica si todos los desafíos I2C están completados y, en caso afirmativo, otorga la recompensa:
                        if (GameDbOperations.Instance.AreAllChallengesCompletedForProtocol(GameProtocolType.I2C_L1))
                        {
                            AreI2cL1ChallengesCompleted = true;
                            string ip = "192.168.X.X"; // reemplaza con la IP real
                            MessageBox.Show($"Has dominado los secretos místicos del I2C. Como recompensa, te otorgamos estas coordenadas estelares: {ip}.");
                        }
                    }
                    else
                    {
                        // verifica si todos los desafíos I2C están completados y, en caso afirmativo, otorga la recompensa:
                        if (GameDbOperations.Instance.AreAllChallengesCompletedForProtocol(GameProtocolType.I2C_L2))
                        {
                            AreI2cL2ChallengesCompleted = true;
                            MessageBox.Show("Aventurero, has demostrado dominar el lenguaje místico de I2C. Ahora, el antiguo artefacto del reino, el Cristal " +
                                "de Comunicación, te espera. Pero ten cuidado, sólo aquellos que realmente dominen el I2C podrán interactuar con él sin romper" +
                                " el vínculo místico. Preguntale quies es (Escribe: IDENTIFY)");
                        }
                    }
                }
                else
                {
                    FeedbackI2C = "Respuesta incorrecta. Intenta nuevamente.";
                }
            }
            catch (InvalidOperationException ex)
            {
                // Mostrar un mensaje de error siguiendo la temática
                MessageBox.Show($"Hechizo fallido: {ex.Message}. Invoca correctamente el espíritu de la respuesta.");
            }
            catch (Exception ex)
            {
                // Para otros errores inesperados
                MessageBox.Show($"Error mágico inesperado: {ex.Message}. Consulta con el Oráculo (soporte técnico).");
            }
        }
    } 
}
