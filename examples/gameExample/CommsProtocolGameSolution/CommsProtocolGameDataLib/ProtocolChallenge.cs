﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

namespace CommsProtocolGameDataLib
{
    public enum GameProtocolType
    {
        I2C_L1,
        I2C_L2,
        SPI_L1,
        SPI_L2,
        UART_L1,
        UART_L2
    }

    public class ProtocolChallenge
    {
        public ObjectId Id { get; set; }
        public string ChallengeName { get; set; }
        public GameProtocolType ProtocolType { get; set; } 
        public string Command { get; set; }
        public string ExpectedResponse { get; set; }
        public string Hint { get; set; }
        public int RewardPoints { get; set; }
        public List<string> ResponseOptions { get; set; }
        public bool IsCompleted { get; set; }
    }

    public class CommandData
    {
        public ObjectId Id { get; set; }  // Id proporcionado por MongoDB
        public string Command { get; set; }
        public Dictionary<string, string> Parameters { get; set; }
        public DateTime Timestamp { get; set; }  // Puedes agregar un timestamp para saber cuándo fue enviado el comando
    }

    public class GameDbOperations
    {
        private static GameDbOperations _instance;
        private static readonly object _lock = new object();

        private IMongoCollection<ProtocolChallenge> _protocolChallengeCollection;
        private IMongoCollection<CommandData> _commandCollection;


        private GameDbOperations()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("JuegoDeComunicacionesDb");
            _protocolChallengeCollection = database.GetCollection<ProtocolChallenge>("protocolChallenges");
            _commandCollection = database.GetCollection<CommandData>("commands");
        }

        public static GameDbOperations Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new GameDbOperations();
                    }
                    return _instance;
                }
            }
        }

        #region commandCollection

        public void SaveCommandToDb(CommandData cmdData)
        {
            _commandCollection.InsertOne(cmdData);
        }

        public CommandData GetCommandByName(string commandName)
        {
            return _commandCollection.Find(command => command.Command == commandName).FirstOrDefault();
        }

        #endregion commandCollection


        public void ResetAdventure()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("JuegoDeComunicacionesDb");
            var collection = database.GetCollection<ProtocolChallenge>("protocolChallenges");

            var update = Builders<ProtocolChallenge>.Update.Set(challenge => challenge.IsCompleted, false);
            var filter = Builders<ProtocolChallenge>.Filter.Empty; // Esto selecciona todos los documentos.

            collection.UpdateMany(filter, update);
        }

        public void DeleteAllDocumentsFromCollection()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("JuegoDeComunicacionesDb");
            var collection = database.GetCollection<ProtocolChallenge>("protocolChallenges");

            var filter = Builders<ProtocolChallenge>.Filter.Empty; // Esto selecciona todos los documentos.
            collection.DeleteMany(filter);
        }


        public bool AreAllChallengesCompletedForProtocol(GameProtocolType protocolType)
        {
            var challengesForProtocol = GetChallengesByType(protocolType);
            return challengesForProtocol.All(challenge => challenge.IsCompleted);
        }

        public void UpdateChallengeStatus(ObjectId challengeId, bool isCompleted)
        {
            var filter = Builders<ProtocolChallenge>.Filter.Eq(c => c.Id, challengeId);
            var update = Builders<ProtocolChallenge>.Update.Set(c => c.IsCompleted, isCompleted);
            _protocolChallengeCollection.UpdateOne(filter, update);
        }

        public void InsertProtocolChallenge(ProtocolChallenge challenge)
        {
            // Antes de insertar, verifica si el desafío ya existe para evitar duplicados.
            var existingChallenge = _protocolChallengeCollection.Find(c => c.ChallengeName == challenge.ChallengeName && c.ProtocolType == challenge.ProtocolType).FirstOrDefault();
            if (existingChallenge == null)
                _protocolChallengeCollection.InsertOne(challenge);
        }

        public List<ProtocolChallenge> GetAllChallenges()
        {
            return _protocolChallengeCollection.Find(challenge => true).ToList();
        }

        // Puedes agregar un método para obtener desafíos por tipo de protocolo
        public List<ProtocolChallenge> GetChallengesByType(GameProtocolType protocolType)
        {
            return _protocolChallengeCollection.Find(challenge => challenge.ProtocolType == protocolType).ToList();
        }

        public ProtocolChallenge GetChallengeByName(string challengeName)
        {
            return _protocolChallengeCollection.Find(challenge => challenge.ChallengeName == challengeName).FirstOrDefault();
        }

        public void UpdateChallenge(ProtocolChallenge challengeToUpdate)
        {
            var filter = Builders<ProtocolChallenge>.Filter.Eq(c => c.Id, challengeToUpdate.Id);
            _protocolChallengeCollection.ReplaceOne(filter, challengeToUpdate);
        }
    }
}
