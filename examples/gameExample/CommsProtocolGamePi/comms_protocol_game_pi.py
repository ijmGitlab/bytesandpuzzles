
import socket
import json

# Definiciones
PORT = 5000
LED_ON_BYTE = 0xA5  # Representación de encender el LED
LED_OFF_BYTE = 0x5A  # Representación de apagar el LED

# Configurar el socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("192.168.0.165", PORT))
server_socket.listen(1)

while True:
    client_socket, address = server_socket.accept()
    data = client_socket.recv(1024).decode('utf-8')

    client_socket.close()