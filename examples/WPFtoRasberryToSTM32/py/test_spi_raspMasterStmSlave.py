
import spidev
import time

spi = spidev.SpiDev()
spi.open(0, 0)  # Bus SPI 0, dispositivo 0
spi.max_speed_hz = 500000  # 500 kHz

def send_data(data):
    response = spi.xfer2([data])
    return response[0]

if __name__ == "__main__":
    try:
        while True:
            response = send_data(0xA5)
            print(f"Sent: 0xA5, Received: {response}")
            time.sleep(1)
    except KeyboardInterrupt:
        spi.close()
        print("Terminado por el usuario.")
