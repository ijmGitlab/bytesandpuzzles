import serial
import time

# Configura el puerto serie según tu sistema.
# Por ejemplo, para Windows puede ser 'COMx' y para Linux '/dev/ttyUSBx' o '/dev/ttyACMx'
PORT = 'COM3'
BAUD_RATE = 115200

# Abre el puerto serie
ser = serial.Serial(PORT, BAUD_RATE, timeout=1)  # timeout=1 significa que esperará hasta 1 segundo por datos
ser.flush()

# Envía un mensaje
test_msg = b'HOLA____'  # 8 bytes, conforme a lo que esperas en tu STM32
ser.write(test_msg)

# Espera un poco para asegurarte de que los datos sean recibidos y enviados de vuelta
time.sleep(0.1)

# Lee los datos de respuesta (loopback)
received_data = ser.read(8)  # espera recibir 8 bytes

print(f"Sent: {test_msg}")
print(f"Received: {received_data}")

# Cierra el puerto serie
ser.close()
