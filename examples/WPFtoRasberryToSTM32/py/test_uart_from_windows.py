import serial
import time

# Configura el puerto serie. Ajusta según tu configuración.
port_name = 'COM3'  # Ajusta este valor al puerto adecuado. Por ejemplo, en Linux podría ser '/dev/ttyUSB0'.
baud_rate = 115200

with serial.Serial(port_name, baud_rate, timeout=1) as ser:
    while True:
        # Enviar un mensaje al STM32
        message = 'H'
        print(f"Sending: {message}")
        ser.write(message.encode('utf-8'))

        time.sleep(1)  # Espera un poco antes de leer la respuesta.

        # Leer la respuesta del STM32 (debería ser el mismo mensaje en caso de loopback).
        response = ser.read(len(message)).decode('utf-8')
        print(f"Received: {response}")

        time.sleep(1)  # Espera un poco antes de enviar el próximo mensaje.



