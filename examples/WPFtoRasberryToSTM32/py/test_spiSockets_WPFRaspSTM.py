import socket
import spidev
import time

# Configuración SPI
spi = spidev.SpiDev()
spi.open(0, 0)  # Bus SPI 0, dispositivo 0
spi.max_speed_hz = 500000  # 500 kHz

def send_data(data):
    response = spi.xfer2([data])
    return response[0]

import socket
import spidev
import time

# Configuración SPI
spi = spidev.SpiDev()
spi.open(0, 0)  # Bus SPI 0, dispositivo 0
spi.max_speed_hz = 500000  # 500 kHz

def send_data(data):
    response = spi.xfer2([data])
    return response[0]

# Configuración Socket
server_ip = '192.168.0.165'
port = 12345

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((server_ip, port))
s.listen(1)

print("Esperando conexiones...")

conn, addr = s.accept()

try:
    while True:
        data = conn.recv(1024)
        if not data: 
            break

        print("Comando recibido:", data.decode('utf-8'))

        # Envía y recibe datos del STM32 a través de SPI
        response_spi = send_data(0xA5)
        print(f"Sent: 0xA5, Received: {response_spi}")

        # Puedes enviar una respuesta al PC si es necesario
        conn.sendall(f"Received from STM32: {response_spi}".encode())

except KeyboardInterrupt:
    pass
finally:
    conn.close()
    spi.close()
    print("Terminado.")
