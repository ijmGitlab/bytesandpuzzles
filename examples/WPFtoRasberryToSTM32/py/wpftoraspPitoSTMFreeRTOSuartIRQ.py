import socket
import serial
import time

# Configuración de socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('192.168.0.165', 12345))
server_socket.listen(1)

# Configuración de UART
ser = serial.Serial('/dev/ttyACM0', 115200)  # Asegúrate de usar el puerto correcto y configurar la velocidad de baudios adecuada

print("Esperando conexiones...")
client_socket, addr = server_socket.accept()

data = client_socket.recv(1024)
if data:
    print("Datos recibidos:", data.decode())
    # Envía los datos a STM a través de UART
    ser.write(data)

    time.sleep(0.1)

    # Espera una respuesta del STM
    response =  ser.read(8) # Asumo que tu respuesta termina en una nueva línea. Ajusta según tu protocolo.
    print("Respuesta de STM:", response.decode())

client_socket.close()
ser.close()
