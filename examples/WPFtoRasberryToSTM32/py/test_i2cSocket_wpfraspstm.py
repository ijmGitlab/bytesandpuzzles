import socket
import smbus

HOST = '192.168.0.165'  # Dirección IP del Raspberry Pi
PORT = 65432              # Puerto para escuchar

bus = smbus.SMBus(1)      # Indica el bus I2C número 1
STM32_ADDRESS = 0x11     # Dirección I2C del STM32

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        while True:
            data = conn.recv(1024)
            data_integer = int.from_bytes(data, "big")
            if not data_integer:
                break
            bus.write_byte_data(STM32_ADDRESS, 0, data_integer)

            received_data = bus.read_byte_data(STM32_ADDRESS, 0x00)

            # Convertir a diferentes formatos, si es necesario
            received_data_bytes = received_data.to_bytes(1, 'big')
            received_data_hex = hex(received_data)

            print("Dato recibido:", received_data)
            print("Dato en bytes:", received_data_bytes)
            print("Dato en hexadecimal:", received_data_hex)
