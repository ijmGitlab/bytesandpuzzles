import serial
import time

PORT = 'COM3'  # Ajusta según tu sistema
BAUD_RATE = 115200

ser = serial.Serial(PORT, BAUD_RATE, timeout=1)

test_msg = b'HOLA____'  # 8 bytes
ser.write(test_msg)

time.sleep(0.1)

received_data = ser.read(8)
print(f"Sent: {test_msg}")
print(f"Received: {received_data}")

ser.close()
