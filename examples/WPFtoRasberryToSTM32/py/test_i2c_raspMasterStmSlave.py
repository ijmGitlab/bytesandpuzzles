import smbus
import time

# Configura el bus I2C y la dirección del esclavo
bus = smbus.SMBus(1)
ADDRESS = 0x11  # La dirección del esclavo STM32 (según tu salida de i2cdetect)

def write_data(value):
    bus.write_byte(ADDRESS, value)
    print(f"Wrote {value} to STM32")
    time.sleep(1)

def read_data():
    data = bus.read_byte(ADDRESS)
    print(f"Received {data} from STM32")
    time.sleep(1)
    return data

if __name__ == "__main__":
    try:
        while True:
            # Escribe un valor al esclavo
            write_data(0xA5)
            
            # Lee un valor desde el esclavo
            read_data()
            
    except KeyboardInterrupt:
        print("Terminado por el usuario.")
