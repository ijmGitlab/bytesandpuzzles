﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private TcpClient client;
        private NetworkStream stream;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                client = new TcpClient("192.168.0.165", 12345);
                stream = client.GetStream();
                byte[] message = Encoding.ASCII.GetBytes("activar_");
                stream.Write(message, 0, message.Length);
            }
            catch (Exception ex)
            {
                // Manejar excepción
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
