﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2spiAndSockets
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string serverIP = "192.168.0.165";
            int port = 12345;

            TcpClient client = new TcpClient(serverIP, port);
            NetworkStream stream = client.GetStream();

            byte[] message = System.Text.Encoding.ASCII.GetBytes("Hello Raspberry");
            stream.Write(message, 0, message.Length);

            stream.Close();
            client.Close();
        }
    }
}
