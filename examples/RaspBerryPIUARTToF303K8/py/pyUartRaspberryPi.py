import serial

# Configura el puerto serial
ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
)

# Lee y muestra los datos recibidos
while True:
    line = ser.readline().decode('ascii')  # Lee una línea desde el puerto serial
    if line:  # Si la línea no está vacía, imprímela
        print(line)


ser = serial.Serial('/dev/serial0', 9600)

try:
    ser.write(b'Hola desde Raspberry Pi 1!\n')
finally:
    ser.close()




