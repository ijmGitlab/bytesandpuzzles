﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace ClassLibrary1
{
    public class SpiData
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Command { get; set; }
        public string Data { get; set; }
        public int DeviceId { get; set; }
    }

    public class SpiDbOperations
    {
        private IMongoCollection<SpiData> _collection;

        public SpiDbOperations()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("spidb");
            _collection = database.GetCollection<SpiData>("spidata");
        }

        public void InsertSpiData(SpiData data)
        {
            _collection.InsertOne(data);
        }

        public SpiData GetSpiDataByCommand(string command)
        {
            var filter = Builders<SpiData>.Filter.Eq("Command", command);
            return _collection.Find(filter).FirstOrDefault();
        }
    }
}
