﻿using ClassLibrary1;
using MongoDB.Bson.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JsonConvert = Newtonsoft.Json.JsonConvert;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private SpiDbOperations _dbOps = new SpiDbOperations();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadDatabase_Click(object sender, RoutedEventArgs e)
        {
            var encenderData = new SpiData
            {
                Command = "encenderLed",
                Data = "ON",
                DeviceId = 1
            };
            _dbOps.InsertSpiData(encenderData);

            var apagarData = new SpiData
            {
                Command = "apagarLed",
                Data = "OFF",
                DeviceId = 1
            };
            _dbOps.InsertSpiData(apagarData);
        }

        private void BtnON_Click(object sender, RoutedEventArgs e)
        {
            var spiData = _dbOps.GetSpiDataByCommand("encenderLed");

            if (spiData != null)
            {
                // Aquí enviarías spiData a través del socket al Raspberry Pi.
                SendDataViaSocket(spiData);
            }
        }

        private void BtnOFF_Click(object sender, RoutedEventArgs e)
        {
            var spiData = _dbOps.GetSpiDataByCommand("apagarLed");

            if (spiData != null)
            {
                // Aquí enviarías spiData a través del socket al Raspberry Pi.
                SendDataViaSocket(spiData);
            }

        }

        private void SendDataViaSocket(SpiData data)
        {
            string dataToSend = JsonConvert.SerializeObject(data);

            using (var client = new TcpClient("192.168.0.165", 12345))
            using (var stream = client.GetStream())
            {
                byte[] dataBytes = Encoding.UTF8.GetBytes(dataToSend);
                stream.Write(dataBytes, 0, dataBytes.Length);
            }
        }
    }
}
