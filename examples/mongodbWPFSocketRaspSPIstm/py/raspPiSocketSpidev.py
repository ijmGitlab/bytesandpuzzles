
import socket
import spidev
import json

# Configurar SPI
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz = 500000  # 500 kHz

# Definiciones
PORT = 12345
LED_ON_BYTE = 0xA5  # Representación de encender el LED
LED_OFF_BYTE = 0x5A  # Representación de apagar el LED

# Configurar el socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("192.168.0.165", PORT))
server_socket.listen(1)

while True:
    client_socket, address = server_socket.accept()
    data = client_socket.recv(1024).decode('utf-8')
    
    if data:
        try:
            # Parsear el JSON recibido
            json_data = json.loads(data)
            
            # Verificar el comando y decidir qué byte enviar
            if 'Command' in json_data:
                if json_data['Command'] == "encenderLed":
                    spi.xfer2([LED_ON_BYTE])
                elif json_data['Command'] == 'apagarLed':
                    spi.xfer2([LED_OFF_BYTE])
                    
        except json.JSONDecodeError:
            print("Error decodificando JSON")

    client_socket.close()
