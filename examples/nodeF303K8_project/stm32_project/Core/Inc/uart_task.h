/*
 * uart_task.h
 *
 *  Created on: Aug 7, 2023
 *      Author: ijm
 */

#ifndef INC_UART_TASK_H_
#define INC_UART_TASK_H_

#include "cmsis_os.h"

#ifdef __cplusplus
extern "C" {
#endif

// Declaración de la función que implementará la tarea UART
void StartUartTask(void *argument);

#ifdef __cplusplus
}
#endif

#endif /* INC_UART_TASK_H_ */
