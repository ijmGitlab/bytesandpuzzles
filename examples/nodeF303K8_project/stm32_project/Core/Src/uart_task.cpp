/*
 * uart_task.cpp
 *
 *  Created on: Aug 7, 2023
 *      Author: ijm
 */

#include "uart_task.h"
#include "main.h" // Asegúrate de que este archivo tenga la configuración adecuada para huart2
#include <cstring> // Incluye esta cabecera para strlen

extern UART_HandleTypeDef huart2;

void StartUartTask(void *argument)
{
  // Bucle infinito
  for(;;)
  {
    const char* message = "Hola desde la tarea UART!\r\n";
    HAL_UART_Transmit(&huart2, (uint8_t*)message, strlen(message), HAL_MAX_DELAY);

    // Retraso de 1 segundo
    osDelay(1000);
  }
}

