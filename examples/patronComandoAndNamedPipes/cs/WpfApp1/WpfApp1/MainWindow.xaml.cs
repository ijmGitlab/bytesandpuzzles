﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Client client = new Client();

            // Envía un comando usando el método en la clase cliente.
            client.SendCommand("Comando para Python");

            MessageBox.Show("Comando enviado!");
        }
    }

    // Interfaz para los comandos
    public interface ICommand
    {
        void Execute();
    }

    public class SendMessageCommand : ICommand
    {
        private string message;

        public SendMessageCommand(string message)
        {
            this.message = message;

        }

        public void Execute()
        {
            // Definir la dirección IP y el puerto para conectar. 
            string ip = "192.168.0.157";
            int port = 5001;

            using (var client = new TcpClient(ip, port))
            using (var writer = new StreamWriter(client.GetStream())) 
            {
                writer.Write(message);
            }
        }
    }

    // Clase cliente que utiliza el comando
    public class Client
    {
        public void SendCommand(string message)
        {
            ICommand command = new SendMessageCommand(message);
            command.Execute();
        }
    }
}
