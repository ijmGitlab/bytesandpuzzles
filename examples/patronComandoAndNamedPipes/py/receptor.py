import socket

class CommandReceiver:
    def execute(self, command):
        print(f"Excuting command: {command}")

# Creación de un Named Pipe
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('192.168.0.157', 5001))
server.listen(1)

while True:
    client, address = server.accept()
    data = client.recv(1024).decode()
    command = CommandReceiver()
    command.execute(data)
    client.close()