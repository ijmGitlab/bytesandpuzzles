﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TcpClient client = new TcpClient("192.168.0.164", 12345);
            NetworkStream stream = client.GetStream();

            byte[] data = Encoding.ASCII.GetBytes("Hola Raspberry Pi!");
            stream.Write(data, 0, data.Length);

            Console.WriteLine("Enviado: Hola Raspberry Pi!");

            data = new byte[256];
            int bytes = stream.Read(data, 0, data.Length);

            string responseData = Encoding.ASCII.GetString(data, 0, bytes);
            Console.WriteLine("Recibido: " + responseData);

            stream.Close();
            client.Close();
        }
    }
}
