import socket

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('192.168.0.164', 12345))
server_socket.listen(1)

print("Esperando conexión...")

client_socket, address = server_socket.accept()
print(f"Conexión desde {address}")

while True:
    data = client_socket.recv(1024)
    if not data:
        break

    print("Recibido:", data.decode())
    client_socket.sendall(data) # Envía los mismos datos de vuelta

client_socket.close()
server_socket.close()
